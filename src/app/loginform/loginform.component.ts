import { Component, OnInit } from '@angular/core';

import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-loginform',
  templateUrl: './loginform.component.html',
  styleUrls: ['./loginform.component.scss']
})
export class LoginformComponent implements OnInit {

  constructor(private userservice: UsersService) { }

  ngOnInit() {
  }

  loginForm(userdata)
  {
    let isValid = this.userservice.login(userdata.value);
    if(isValid)
    {
      alert("Hello "+userdata.value.username+"!");
    }
    else
    {
      alert("Sorry! Invalid credentials. Please Try again");
      userdata.reset();
    }
  }

}
