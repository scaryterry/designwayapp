import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import {UsersService} from '../services/users.service';

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.scss']
})
export class EditprofileComponent implements OnInit {

  constructor(private route: ActivatedRoute, private userservice: UsersService) { }

  userdata;
  ngOnInit() {
    this.userdata = this.userservice.getUser();
  }

  id = +this.route.snapshot.params["id"];

  updateDetails(updatedata)
  {
    this.userservice.updateDetails(updatedata.value,this.id);
  }

  
}
