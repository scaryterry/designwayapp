import { Component, OnInit } from '@angular/core';

import {UsersService} from "../services/users.service";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  constructor(private userservice: UsersService) { }

  ngOnInit() {
  }

  onRegister(userdata)
  {
    let USERS = this.userservice.registerUser(userdata.value);
    console.log("USERS:",USERS);
    alert("Registered!");
    userdata.reset();
  }
}
