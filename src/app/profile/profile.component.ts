import { Component, OnInit } from '@angular/core';

import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  user;
  constructor(private userservice: UsersService) { }

  ngOnInit() {
    //let user1="user1";
    this.user = this.userservice.getUser();
    //console.log(this.user.username);
  }
  

}
