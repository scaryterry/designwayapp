import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {FormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginformComponent } from './loginform/loginform.component';
import { ProfileComponent } from './profile/profile.component';
import { EditprofileComponent } from './editprofile/editprofile.component';
import { HeaderComponent } from './header/header.component';

import { routes} from './app-routing/routes';

@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    LoginformComponent,
    ProfileComponent,
    HeaderComponent,
    EditprofileComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
