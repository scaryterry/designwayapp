import {Routes} from '@angular/router';

import {LoginformComponent} from "../loginform/loginform.component";
import {RegistrationComponent} from "../registration/registration.component";
import {ProfileComponent} from "../profile/profile.component";
import {EditprofileComponent} from "../editprofile/editprofile.component";

export const routes: Routes = [
    {path:'login', component:LoginformComponent},
    {path:'register', component: RegistrationComponent},
    {path: 'profile', component: ProfileComponent},
    {path: 'edit/:id', component: EditprofileComponent},
    {path: '**', component:RegistrationComponent},
    {path:'', redirectTo:'/register', pathMatch: 'full'}
]